variable "stream_names" { type = "list" }
variable "environment" { }

module "kinesis_streams" {
  source        = "../kinesis-streams"
  stream_names  = "${var.stream_names}"
  environment   = "${var.environment}"
}
