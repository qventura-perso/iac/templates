variable "stream_names" { type = "list" }
variable "environment" { }
variable "lambda_arn" { }

module "kinesis_streams" {
  source        = "../kinesis-streams"
  stream_names  = "${var.stream_names}"
  environment   = "${var.environment}"
}

resource "aws_lambda_event_source_mapping" "event_source_mapping" {
  count             = "${length(var.stream_names)}"
  event_source_arn  = "${element(module.kinesis_streams.stream_arns, count.index)}"
  batch_size        = 100
  enabled           = true
  function_name     = "${var.lambda_arn}"
  starting_position = "TRIM_HORIZON"
}
