variable "aws_region" { default = "ca-central-1" }
variable "environment" {}
variable "global_env_remote_state_key" {}
variable "global_remote_state_key" {}
variable "has_mongo_uri" { default = false }
variable "memory_reservation" { default = 150 }
variable "org_name" {}
variable "remote_state_bucket" {}
variable "remote_state_region" {}
variable "sentry_org_name" {}
variable "sentry_token" {}
variable "service_name" {}

provider "sentry" {
  token = "${var.sentry_token}"
}

data "aws_ssm_parameter" "base_service_discovery_url" {
  name = "/Global/${var.environment}/BaseApiHost"
}

resource "aws_ssm_parameter" "service_discovery_url" {
  name        = "/ServiceDiscovery/${var.environment}/ServiceUrls/${replace(replace(title(var.service_name), "-", ""), "_", "")}"
  type        = "String"
  overwrite   = "true"
  value       = "${data.aws_ssm_parameter.base_service_discovery_url.value}/${var.service_name}"
}

data "aws_ssm_parameter" "mongo_uri_template" {
  count       = "${var.has_mongo_uri ? 1 : 0}"
  name = "/Global/${var.environment}/MongoUri"
}

resource "aws_ssm_parameter" "mongo_uri" {
  count       = "${var.has_mongo_uri ? 1 : 0}"
  name        = "/${replace(replace(title(var.service_name), "-", ""), "_", "")}/${var.environment}/MongoUri"
  type        = "String"
  overwrite   = "true"
  value       = "${replace(data.aws_ssm_parameter.mongo_uri_template.value, "$${databaseName}", var.service_name)}"
}

resource "sentry_project" "default" {
  organization = "${var.sentry_org_name}"
  team = "${var.sentry_org_name}"
  name = "${replace(replace(title(var.service_name), "-", ""), "_", "")} ${var.environment}"
}

resource "sentry_key" "default" {
  depends_on  = ["sentry_project.default"]
  organization = "${sentry_project.default.organization}"
  project = "${sentry_project.default.slug}"
  name = "default"
}

resource "aws_ssm_parameter" "sentry_dns" {
  depends_on  = ["sentry_key.default"]
  name        = "/${replace(replace(title(var.service_name), "-", ""), "_", "")}/${var.environment}/SentryDSN"
  type        = "String"
  overwrite   = "true"
  value       = "${sentry_key.default.dsn_secret}"
}
