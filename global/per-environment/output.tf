output "log_group_name" {
  value = "${module.logging.log_group_name}"
}

output "alb_target_group_arn" {
  value = "${module.ecs_microservice.alb_target_group_arn}"
}

output "alb_arn" {
  value = "${module.ecs_microservice.alb_arn}"
}

output "cluster_arn" {
  value = "${module.ecs_microservice.cluster_arn}"
}

output "vpc_id" {
  value = "${module.ecs_microservice.vpc_id}"
}

output "autoscaling_group_name" {
  value = "${module.ecs_microservice.autoscaling_group_name}"
}
