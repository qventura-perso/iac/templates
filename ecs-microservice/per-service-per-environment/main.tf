variable "aws_region" {}
variable "environment" {}
variable "global_env_remote_state_key" {}
variable "memory_reservation" { default = 100 }
variable "remote_state_bucket" {}
variable "service_name" {}

data "terraform_remote_state" "env_global" {
  backend = "s3"

  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.global_env_remote_state_key}"
    region = "${var.aws_region}"
  }
}

data "aws_ecs_task_definition" "service_task_definition" {
  depends_on = ["aws_ecs_task_definition.service_task_definition"]
  task_definition = "${aws_ecs_task_definition.service_task_definition.family}"
}

data "aws_ecr_repository" "service" {
  name = "${var.service_name}"
}

resource "aws_ecs_task_definition" "service_task_definition" {
  family = "${var.service_name}-${var.environment}"

  container_definitions = <<DEFINITION
[
  {
    "environment": [{
      "name": "NODE_ENV",
      "value": "${var.environment}"
    }, {
      "name": "AWS_REGION",
      "value": "${var.aws_region}"
    }],
    "portMappings": [
      {
        "containerPort": 5000
      }
    ],
    "essential": true,
    "image": "${data.aws_ecr_repository.service.repository_url}:${var.environment}-latest",
    "memoryReservation": ${var.memory_reservation},
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${data.terraform_remote_state.env_global.log_group_name}",
        "awslogs-region": "${var.aws_region}"
      }
    },
    "name": "${var.service_name}-${var.environment}"
  }
]
DEFINITION
}

module "target_group" {
  source                  = "../modules/target_groups"
  vpc_id                  = "${data.terraform_remote_state.env_global.vpc_id}"
  environment             = "${var.environment}"
  target_group_name       = "${var.service_name}-${var.environment}"
  autoscaling_group_name  = "${data.terraform_remote_state.env_global.autoscaling_group_name}"
  health_check_path       = "/${var.service_name}/health"
}

resource "aws_ecs_service" "service" {
  lifecycle {
    create_before_destroy = true
    ignore_changes        = ["task_definition"]
  }

  name                              = "${var.service_name}-${var.environment}"
  cluster                           = "${data.terraform_remote_state.env_global.cluster_arn}"
  task_definition                   = "${aws_ecs_task_definition.service_task_definition.family}:${max("${aws_ecs_task_definition.service_task_definition.revision}", "${data.aws_ecs_task_definition.service_task_definition.revision}")}"
  health_check_grace_period_seconds = 300
  desired_count                     = 1

  load_balancer {
    target_group_arn = "${module.target_group.arn}"
    container_name = "${var.service_name}-${var.environment}"
    container_port = 5000
  }

}

data "aws_lb_listener" "load_balancer_listener_80" {
  load_balancer_arn = "${data.terraform_remote_state.env_global.alb_arn}"
  port = 80
}

module "aws_lb_listener_rule_80" {
  source = "../modules/lb_listener_rule"
  load_balancer_listener_arn  = "${data.aws_lb_listener.load_balancer_listener_80.arn}"
  path_pattern                = "/${var.service_name}/*"
  target_group_arn            = "${module.target_group.arn}"
}

data "aws_lb_listener" "load_balancer_listener_443" {
  load_balancer_arn = "${data.terraform_remote_state.env_global.alb_arn}"
  port = 443
}

module "aws_lb_listener_rule_443" {
  source                      = "../modules/lb_listener_rule"
  load_balancer_listener_arn  = "${data.aws_lb_listener.load_balancer_listener_443.arn}"
  path_pattern                = "/${var.service_name}/*"
  target_group_arn            = "${module.target_group.arn}"
}
