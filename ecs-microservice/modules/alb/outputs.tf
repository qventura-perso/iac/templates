output "alb_security_group_id" {
  value = "${aws_security_group.alb.id}"
}

output "default_alb_target_group" {
  value = "${module.target_group_default.arn}"
}

output "arn" {
  value = "${aws_alb.alb.arn}"
}
