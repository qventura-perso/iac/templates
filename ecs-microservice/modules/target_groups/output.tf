output "arn" {
  value = "${aws_alb_target_group.selected.arn}"
}

output "id" {
  value = "${aws_alb_target_group.selected.id}"
}
