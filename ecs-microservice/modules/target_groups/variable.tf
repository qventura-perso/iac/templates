variable "target_group_name" {
  default     = "default"
  description = "The name of the target group"
}

variable "environment" {
  description = "The name of the environment"
}

variable "autoscaling_group_name" {
  description = "The name of the autoscaling group"
}

variable "vpc_id" {
  description = "The VPC id"
}

variable "deregistration_delay" {
  default     = "300"
  description = "The default deregistration delay"
}

variable "health_check_path" {
  default     = "/"
  description = "The default health check path"
}
