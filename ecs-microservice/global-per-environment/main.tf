variable "domain" {}
variable "ecs_aws_ami" { default = "ami-5ac94e3e" }
variable "ecs_availability_zones" { default = [ "ca-central-1a", "ca-central-1b" ]}
variable "ecs_desired_capacity" { default = "2" }
variable "ecs_instance_max_size" { default = "2" }
variable "ecs_instance_min_size" { default = "2" }
variable "ecs_instance_type" { default = "t2.small" }
variable "ecs_private_subnet_cidrs" { default = ["10.0.50.0/24", "10.0.51.0/24"] }
variable "ecs_public_subnet_cidrs" { default = ["10.0.0.0/24", "10.0.1.0/24"] }
variable "ecs_vpc_cidr" { default = "10.0.0.0/16" }
variable "environment" {}

data "aws_acm_certificate" "selected" {
  domain   = "${var.domain}"
  statuses = ["ISSUED"]
}

resource "aws_key_pair" "ecs" {
  key_name   = "ecs-key-${var.environment}"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCtMljjj0Ccxux5Mssqraa/iHHxheW+m0Rh17fbd8t365y9EwBn00DN/0PjdU2CK6bjxwy8BNGXWoUXiSDDtGqRupH6e9J012yE5kxhpXnnkIcLGjkAiflDBVV4sXS4b3a2LSXL5Dyb93N2GdnJ03FJM4qDJ8lfDQxb38eYHytZkmxW14xLoyW5Hbyr3SXhdHC2/ecdp5nLNRwRWiW6g9OA6jTQ3LgeOZoM6dK4ltJUQOakKjiHsE+jvmO0hJYQN7+5gYOw0HHsM+zmATvSipAWzoWBWcmBxAbcdW0R0KvCwjylCyRVbRMRbSZ/c4idZbFLZXRb7ZJkqNJuy99+ld41 dev@mynewknow.com"
}

module "ecs" {
  source     = "../modules/ecs"
  environment          = "${var.environment}"
  cluster              = "microservices"
  vpc_cidr             = "${var.ecs_vpc_cidr}"
  public_subnet_cidrs  = "${var.ecs_public_subnet_cidrs}"
  private_subnet_cidrs = "${var.ecs_private_subnet_cidrs}"
  availability_zones   = "${var.ecs_availability_zones}"
  max_size             = "${var.ecs_instance_max_size}"
  min_size             = "${var.ecs_instance_min_size}"
  desired_capacity     = "${var.ecs_desired_capacity}"
  key_name             = "${aws_key_pair.ecs.key_name}"
  instance_type        = "${var.ecs_instance_type}"
  ecs_aws_ami          = "${var.ecs_aws_ami}"
  cloudwatch_prefix    = "${var.environment}"
  certificate_arn      = "${data.aws_acm_certificate.selected.arn}"
}
