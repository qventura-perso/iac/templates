variable "service_name" {}

resource "aws_elastic_beanstalk_application" "application" {
  name = "${var.service_name}"
}
